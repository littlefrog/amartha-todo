import 'package:flutter/material.dart';

final navigationKey = GlobalKey<NavigatorState>();

abstract class NavigationService {
  Future<dynamic> push(Widget widget);
  Future<dynamic> replace(Widget widget);
  void pop(dynamic result);
  Future<dynamic> showPopup(Widget widget);
}

class NavigationServiceImpl implements NavigationService {
  @override
  Future<dynamic> push(Widget widget) async {
    final context = navigationKey.currentContext;
    if (context == null) return null;
    return Navigator.of(context).push(MaterialPageRoute(builder: (_) => widget));
  }

  @override
  void pop(dynamic result) {
    final context = navigationKey.currentContext;
    if (context == null) return;
    Navigator.of(context).pop(result);
  }

  @override
  Future<dynamic> replace(Widget widget) async {
    final context = navigationKey.currentContext;
    if (context == null) return;
    return Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => widget));
  }

  @override
  Future<dynamic> showPopup(Widget widget) async {
    final context = navigationKey.currentContext;
    if (context == null) return null;
    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (_) => widget);
  }
}
