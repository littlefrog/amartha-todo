import 'package:flutter/material.dart';
import 'package:todo_list/model/model.dart';
import 'package:todo_list/service/service.dart';
import 'dart:math';

class InputDialog extends StatefulWidget {
  const InputDialog({Key? key}) : super(key: key);

  @override
  State<InputDialog> createState() => _InputDialogState();
}

class _InputDialogState extends State<InputDialog> {
  final NavigationService routerService = NavigationServiceImpl();
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(12)),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Task Description',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 8),
            TextField(
              controller: controller,
              textCapitalization: TextCapitalization.words,
              autocorrect: false,
              enableSuggestions: false,
              autofocus: true,
              autofillHints: null,
              style: Theme.of(context).textTheme.titleMedium,
              decoration: const InputDecoration(isDense: true),
            ),
            const SizedBox(height: 24),
            GestureDetector(
              onTap: () async {
                if (controller.text.isEmpty) {
                  return;
                }
                final id = Random().nextDouble().toString();
                routerService.pop(TodoModel(
                    id: id,
                    description: controller.text,
                    isChecked: false,
                    createdAt: DateTime.now()));
              },
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.blue.shade800, borderRadius: BorderRadius.circular(12)),
                child: Text(
                  'Add New Task',
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium
                      ?.copyWith(color: Colors.white, fontWeight: FontWeight.w500),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
          ],
        ),
      ),
    );
  }
}
