import 'package:flutter/material.dart';
import 'package:todo_list/service/router_services.dart';
import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final NavigationService routerService = NavigationServiceImpl();
  final splashScreenDuration = const Duration(seconds: 2);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      Future.delayed(splashScreenDuration).then((_) => routerService.replace(const HomeScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade700,
      body: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(Icons.list, size: 24, color: Colors.white),
            const SizedBox(width: 8),
            Text(
              'Todo App',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
