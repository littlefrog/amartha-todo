import 'package:flutter/material.dart';
import 'package:todo_list/model/model.dart';

class TodoItemView extends StatelessWidget {
  final TodoModel model;
  const TodoItemView({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final style = Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w600);
    return Card(
      margin: const EdgeInsets.only(top: 8, bottom: 12),
      elevation: 1.5,
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    model.description,
                    style: model.isChecked
                        ? style?.copyWith(decoration: TextDecoration.lineThrough)
                        : style,
                  ),
                  const SizedBox(height: 4),
                  Text(
                    model.timeFormatted,
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.grey),
                  ),
                ],
              ),
            ),
            model.isChecked
                ? Container(
                    height: 24,
                    width: 24,
                    decoration: BoxDecoration(
                      color: Colors.blue.shade800,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: const Icon(Icons.check, color: Colors.white),
                  )
                : Container(
                    height: 24,
                    width: 24,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey, width: 2),
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
