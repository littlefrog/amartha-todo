import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_list/bloc/bloc.dart';
import 'package:todo_list/model/model.dart';
import 'package:todo_list/view/input_dialog.dart';

import 'todo_itemview.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final viewModel = HomeBloc();

  TextTheme get textTheme => Theme.of(context).textTheme;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.grey.shade50,
        appBar: PreferredSize(
            preferredSize: Size.zero,
            child: AppBar(centerTitle: false, elevation: 0, backgroundColor: Colors.grey.shade50)),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header,
            content,
          ],
        ));
  }

  Widget get content => Expanded(
        child: BlocProvider(
          create: (_) => viewModel,
          child: BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
            if (state.todoList.isEmpty) {
              return Center(
                child: Text(
                  'You have no task yet',
                  style: textTheme.bodyLarge?.copyWith(fontWeight: FontWeight.w600),
                ),
              );
            }
            return ListView(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
              children: state.todoList.map((todoModel) => GestureDetector(
                  onTap: (){
                    viewModel.add(TapTodoList(todoModel: todoModel));
                  },
                  child: TodoItemView(model: todoModel))).toList(),
            );
          }),
        ),
      );

  Widget get header => Container(
        margin: const EdgeInsets.only(top: 32),
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Today's Task",
                  style: textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 4),
                Text(
                  viewModel.today,
                  style: textTheme.titleSmall
                      ?.copyWith(color: Colors.grey, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            GestureDetector(
              onTap: () async {
                final result = await viewModel.routerService.showPopup(const InputDialog());
                if (result is TodoModel) {
                  viewModel.add(AddTodoList(todoModel: result));
                }
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                decoration: BoxDecoration(
                    color: Colors.blue.shade800.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(12)),
                child: Row(
                  children: [
                    Icon(Icons.add, color: Colors.blue.shade800),
                    const SizedBox(width: 8),
                    Text(
                      'New Task',
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(color: Colors.blue.shade800, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ),
            )
          ],
        ),
      );
}
