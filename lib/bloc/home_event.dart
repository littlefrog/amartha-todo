import 'package:equatable/equatable.dart';
import 'package:todo_list/model/model.dart';

abstract class HomeEvent extends Equatable {}

class AddTodoList extends HomeEvent {
  final TodoModel todoModel;

  AddTodoList({required this.todoModel});

  @override
  List<Object?> get props => [todoModel.description, todoModel.isChecked];
}

class TapTodoList extends HomeEvent {
  final TodoModel todoModel;

  TapTodoList({required this.todoModel});

  @override
  List<Object?> get props =>
      [todoModel.id, todoModel.description, todoModel.isChecked, todoModel.createdAt];
}
