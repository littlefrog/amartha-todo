import 'package:equatable/equatable.dart';
import 'package:todo_list/model/model.dart';

class HomeState extends Equatable {
  final List<TodoModel> todoList;

  const HomeState({required this.todoList});

  @override
  List<Object?> get props => [todoList];

  HomeState copyWith({List<TodoModel>? todoList}) {
    return HomeState(todoList: todoList ?? this.todoList);
  }
}
