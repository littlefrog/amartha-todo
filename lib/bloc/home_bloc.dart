import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_list/bloc/home_event.dart';
import 'package:todo_list/bloc/home_state.dart';
import 'package:todo_list/model/model.dart';
import 'package:todo_list/service/service.dart';
import 'package:intl/intl.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final NavigationService routerService = NavigationServiceImpl();

  String get today => DateFormat('EEEE, d MMMM').format(DateTime.now());

  HomeBloc() : super(const HomeState(todoList: [])) {
    on<AddTodoList>(_onAddTodoList);
    on<TapTodoList>(_onTapTodoList);
  }

  Future<void> _onAddTodoList(AddTodoList event, Emitter<HomeState> emit) async {
    emit(state.copyWith(todoList: state.todoList + [event.todoModel]));
  }

  Future<void> _onTapTodoList(TapTodoList event, Emitter<HomeState> emit) async {
    List<TodoModel> updatedList = state.todoList.map((todo) {
      if (todo == event.todoModel) {
        return todo.copyWith(isChecked: !event.todoModel.isChecked);
      }
      return todo;
    }).toList();
    emit(state.copyWith(todoList: updatedList));
  }
}
