# Todo App

A new Todo list project build with Flutter.
This project is a starting point for a Flutter application.

## Tech Stack
- BLoC State Management
- Freezed Model Generator
- Mockito Mocking
- Unit Test Ready
- Flutter version 3.7.0 • channel stable

## Screen Shot
| Splash Screen | Empty State | Input Task | Normal State | Selected State |
|---------------|-------------|------------|--------------|----------------|
| ![Screenshot_2023-05-21_at_15.59.22](/uploads/751d8274fbdcd3e75e549c7b7f0caa8b/Screenshot_2023-05-21_at_15.59.22.png) | ![Screenshot_2023-05-21_at_15.58.56](/uploads/c02a4ea5fd518b6d4715122bcf80f4b0/Screenshot_2023-05-21_at_15.58.56.png) | ![Screenshot_2023-05-21_at_15.58.36](/uploads/457ef23e9f20a2871fd0005a83295a2d/Screenshot_2023-05-21_at_15.58.36.png) | ![Screenshot_2023-05-21_at_15.58.24](/uploads/5c0202a8ca8861e3c4b37441759f6547/Screenshot_2023-05-21_at_15.58.24.png) | ![Screenshot_2023-05-21_at_15.58.08](/uploads/745ec71b153a96d0ec4daf5fd61c267e/Screenshot_2023-05-21_at_15.58.08.png) |
