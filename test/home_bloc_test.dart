import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_list/bloc/bloc.dart';
import 'package:todo_list/model/model.dart';
import 'package:todo_list/service/service.dart';

// Mock the RouterService
class MockRouterService extends Mock implements NavigationService {}

void main() {
  group('HomeBloc', () {
    late HomeBloc homeBloc;

    setUp(() {
      // Create an instance of HomeBloc before each test
      homeBloc = HomeBloc();
    });

    tearDown(() {
      // Close the HomeBloc after each test
      homeBloc.close();
    });

    test('initial state is correct', () {
      expect(homeBloc.state, const HomeState(todoList: []));
    });

    final id = Random().nextDouble().toString();
    final todoModel =
        TodoModel(id: id, description: '', isChecked: false, createdAt: DateTime.now());

    blocTest<HomeBloc, HomeState>(
      'emits updated state when AddTodoList event is added',
      build: () => homeBloc,
      act: (bloc) => bloc.add(AddTodoList(todoModel: todoModel)),
      expect: () => [
        // Verify that todoList is updated with the new todoModel
        isA<HomeState>().having((state) => state.todoList.length, 'todoList.length', 1),
      ],
    );

    final addedState = HomeState(todoList: [todoModel]);
    blocTest<HomeBloc, HomeState>(
      'emits updated state when TapTodoList event is added',
      build: () => homeBloc,
      seed: () => addedState,
      act: (bloc) => [bloc.add(TapTodoList(todoModel: todoModel))],
      expect: () => [
        // Verify that todoList is updated with the toggled isChecked value
        isA<HomeState>().having(
            (state) => state.todoList.first.isChecked, 'tappedTodo.isChecked', !todoModel.isChecked)
      ],
    );
  });
}